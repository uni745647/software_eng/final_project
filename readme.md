### RemeDay: your App for tracking medicines

> O documento descreve o projeto de um aplicativo chamado REMEDAY, que visa facilitar o gerenciamento de medicamentos, humor e sintomas diários dos usuários. O aplicativo surge da necessidade de um cliente real e será desenvolvido seguindo alguma das metodologias ágeis. O documento tem como objetivo documentar todo o app para orientar o desenvolvimento e o entendimento dos envolvidos no projeto. O documento define o conceito de software como um conjunto de instruções com o objetivo de atingir um propósito específico, de modo a atender as necessidades da humanidade. 

* Palavras-chave: REMEDAY, gerenciamento de medicação diária, desenvolvimento de software, metodologias ágeis, documentação de software, app.